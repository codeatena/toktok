package com.buddy.service;

import java.util.HashMap;
import java.util.Map;

import com.buddy.sdk.Buddy;
import com.buddy.sdk.BuddyCallback;
import com.buddy.sdk.BuddyResult;
import com.chatt.GlobalData;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import android.content.Context;

public class BuddyDeviceService extends BuddyService {

	public BuddyDeviceService(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	public BuddyDeviceService(Context context, String _title) {
		super(context, _title);
	}
	
	public void updateDevice(String pushToken, String connectionType, boolean isProduction, String tag) {
		Map<String,Object> parameters = new HashMap<String,Object>();
		 
		if (pushToken != null) 			parameters.put("pushToken", pushToken);
		if (connectionType != null) 	parameters.put("connectionType", 	connectionType);
//		if (isProduction != null) 		parameters.put("isProduction", 		isProduction);
		if (tag != null) 				parameters.put("tag", tag);
		
		double lat = GlobalData.currentLocation.getLatitude();
		double lng = GlobalData.currentLocation.getLongitude();
		
		parameters.put("location", Double.toString(lat) + "," + Double.toString(lng));

		Buddy.<JsonObject>patch("/devices/current", parameters, new BuddyCallback<JsonObject>(JsonObject.class) {
		    @Override
		    public void completed(BuddyResult<JsonObject> result) {
				if (dlgLoading.isShowing())
					dlgLoading.dismiss();
				
		        if (result.getIsSuccess()) {
		        	
		        }
		    }
		});
	}
}
