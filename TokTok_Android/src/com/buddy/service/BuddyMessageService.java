package com.buddy.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.buddy.sdk.Buddy;
import com.buddy.sdk.BuddyCallback;
import com.buddy.sdk.BuddyResult;
import com.buddy.sdk.models.User;
import com.chatt.GlobalData;
import com.chatt.Login;
import com.chatt.MainActivity;
import com.chatt.model.ChatMessage;
import com.chatt.model.ChatRoom;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.han.utility.DialogUtility;
import com.han.utility.TimeUtility;

import android.content.Context;
import android.util.Log;

public class BuddyMessageService extends BuddyService {
	
	public BuddyMessageService(Context context) {
		super(context);
	}
	
	public BuddyMessageService(Context context, String _title) {
		super(context, _title);
	}
	
	public BuddyMessageService(Context context, String _title, boolean isShowProgressDialog) {
		super(context, _title, isShowProgressDialog);
	}
	
	public BuddyMessageService(Context context, boolean isShowProgressDialog) {
		super(context, isShowProgressDialog);
	}
	
	public void sendMessage(ArrayList<User> arrToUser, String subject, String body, String inResponseTo, String thread) {
		
		Map<String,Object> parameters = new HashMap<String,Object>();
		
		String strAddresses = "[\"";
		for (int i = 0; i < arrToUser.size(); i++) {
		   
		   User user = arrToUser.get(i);
		   if (i == 0) {
			   strAddresses += user.id;
		   } else {
			   strAddresses += "\",\"" + user.id;
		   }
		}
		strAddresses += "\"]";
		
		Log.e("Addresses", strAddresses);
		
		parameters.put("addressees", strAddresses);
		parameters.put("subject", "message");
		
		if (body != null) 			parameters.put("body", body);
		if (inResponseTo != null) 	parameters.put("inResponseTo", inResponseTo);
		if (thread != null)			parameters.put("thread", thread);
		
		double lat = GlobalData.currentLocation.getLatitude();
		double lng = GlobalData.currentLocation.getLongitude();
		
		parameters.put("location", Double.toString(lat) + "," + Double.toString(lng));

		Buddy.<JsonObject>post("/messages", parameters, new BuddyCallback<JsonObject>(JsonObject.class) {
		    @Override
		    public void completed(BuddyResult<JsonObject> result) {
		    	
				if (dlgLoading.isShowing())
					dlgLoading.dismiss();
				
		        if (result.getIsSuccess()) {
		            JsonObject obj = result.getResult();
		        }
		    }
		});
	}
	
	public void searchMessageInRoom(ChatRoom room) {
		Map<String,Object> parameters = new HashMap<String,Object>();
		parameters.put("thread", room.roomID);
		parameters.put("sortOrder", "+created");

		Buddy.<JsonObject>get("/messages", parameters, new BuddyCallback<JsonObject>(JsonObject.class) {
		    @Override
		    public void completed(BuddyResult<JsonObject> result) {
				if (dlgLoading.isShowing())
					dlgLoading.dismiss();
				
		        if (result.getIsSuccess()) {
		        	try {
		        		JsonObject jsonResult = result.getResult();
		        		JsonArray jsonArrMessage = jsonResult.get("pageResults").getAsJsonArray();
		        		
		        		ArrayList<ChatMessage> arrChatMessage = new ArrayList<ChatMessage>();
		        		
		        		for (int i = 0; i < jsonArrMessage.size(); i++) {
		        			JsonObject jsonMessage = jsonArrMessage.get(i).getAsJsonObject();
		        			ChatMessage chatMessage = new ChatMessage();
		        			chatMessage.id = jsonMessage.get("id").getAsString();
		        			chatMessage.body = jsonMessage.get("body").getAsString();
		        			chatMessage.subject = jsonMessage.get("subject").getAsString();
		        			String senderID = jsonMessage.get("from").getAsString();
		        			chatMessage.sender = GlobalData.getUser(senderID);
		        			
		        			chatMessage.dtCreated = TimeUtility.getDateFromJsonString(jsonMessage.get("created").getAsString());
		        			chatMessage.dtSent = TimeUtility.getDateFromJsonString(jsonMessage.get("sent").getAsString());
		        			chatMessage.dtLastModified = TimeUtility.getDateFromJsonString(jsonMessage.get("lastModified").getAsString());
		        			
		        			//chatMessage.created = 
		        			arrChatMessage.add(chatMessage);
		        		}
		        		
		        		MainActivity mainActivity = (MainActivity) parent;
		        		mainActivity.successSearchMessage(arrChatMessage);
		        		
		        	} catch (Exception e) {
		        		
		        	}
		        }
		    }
		});
	}
	
	public void searchMessagesUnread() {
		Map<String,Object> parameters = new HashMap<String,Object>();
		parameters.put("isNew", true);
		parameters.put("sortOrder", "+created");
		
		Buddy.<JsonObject>get("/messages", parameters, new BuddyCallback<JsonObject>(JsonObject.class) {
		    @Override
		    public void completed(BuddyResult<JsonObject> result) {
				if (dlgLoading.isShowing())
					dlgLoading.dismiss();
				
		        if (result.getIsSuccess()) {
		        	try {
		        		
		        	} catch (Exception e) {
		        		
		        	}
		        }
		    }
		});
	}
}
