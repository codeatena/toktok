package com.buddy.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.buddy.sdk.Buddy;
import com.buddy.sdk.BuddyCallback;
import com.buddy.sdk.BuddyResult;
import com.buddy.sdk.models.User;
import com.chatt.GlobalData;
import com.chatt.MainActivity;
import com.chatt.model.ChatRoom;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import android.content.Context;
import android.util.Log;

public class BuddyMetaDataService extends BuddyService {
	
	public String metaDataValue;
	public User curUser;
	public ChatRoom curRoom;
	
	public BuddyMetaDataService(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public BuddyMetaDataService(Context context, String _title) {
		super(context, _title);
		// TODO Auto-generated constructor stub
	}
	
	public void createRoomMetaData(ChatRoom chatRoom, User user) {
		
//		arrUser.add(GlobalData.currentUser);
		curUser = user;
		curRoom = chatRoom;
		getRoomMetaData(user);
	}
	
	private void addUserMetaData(ChatRoom chatRoom, User user, String values) {
		
		Map<String,Object> parameters = new HashMap<String,Object>();

		if (values.equals("")) {
			values += chatRoom.roomID;
		} else {
			values += "," + chatRoom.roomID; 
		}
		
		parameters.put("value", values);
		parameters.put("location", GlobalData.getCurrentLocationString());
		
		String mykey = user.id + "_mylist";
		
		Buddy.<JsonObject>put("/metadata/app/" + mykey, parameters, new BuddyCallback<JsonObject>(JsonObject.class) {
		    @Override
		    public void completed(BuddyResult<JsonObject> result) {
		    	
				if (dlgLoading.isShowing())
					dlgLoading.dismiss();
				
		        if (result.getIsSuccess()) {
		        	
		        }
		    }
		});
	}
	
	public void getRoomMetaData(User user) {
		Map<String,Object> parameters = new HashMap<String,Object>();
		
		String key = user.id + "_mylist";
		
		Buddy.<JsonObject>get("/metadata/app/" + key, parameters, new BuddyCallback<JsonObject>(JsonObject.class) {
		    @Override
		    public void completed(BuddyResult<JsonObject> result) {
			
				if (dlgLoading.isShowing())
					dlgLoading.dismiss();
				
		        if (result.getIsSuccess()) {
		        	JsonObject jsonResult = result.getResult();
		        	String value = jsonResult.get("value").getAsString();
		        	if (value == null) {
		        		value = "";
		        	}
		        	metaDataValue = value;
		        	
		        } else {
		        	metaDataValue = "";
		        }
		        
		        Log.e(curUser.id + " value", metaDataValue);
	        	addUserMetaData(curRoom, curUser, metaDataValue);
		    }
		});
	}
	
	public void getMyRoomMetaData() {
		Map<String,Object> parameters = new HashMap<String, Object>();
		
		String key = GlobalData.currentUser.id + "_mylist";
		
		parameters.put("key", key);
		Buddy.<JsonObject>get("/metadata/app/" + key, parameters, new BuddyCallback<JsonObject>(JsonObject.class) {
		    @Override
		    public void completed(BuddyResult<JsonObject> result) {
		    	
				if (dlgLoading.isShowing())
					dlgLoading.dismiss();
				
		        if (result.getIsSuccess()) {

		        	JsonObject jsonResult = result.getResult();
		        	
		        	String value = "";
		        	JsonElement jsonValue = jsonResult.get("value");
		        	if (jsonValue != null) {
		        		value = jsonValue.getAsString();
		        	}
		        	
		        	Log.e("room ids", value);
		        	MainActivity mainActivity = (MainActivity) parent;
		        	mainActivity.successGetMyRoomMetaData(value);
		        	
//		        	JsonArray jsonArrRoom = jsonResult.get("pageResults").getAsJsonArray();
//		        	
//		        	GlobalData.arrMyRoom = new ArrayList<ChatRoom>();
//		        	
//	        		for (int i = 0; i < jsonArrRoom.size(); i++) {
//	        			
//	        			JsonObject jsonChatRoom = jsonArrRoom.get(i).getAsJsonObject();
//			        	ChatRoom room = new ChatRoom();
//			        	String key = jsonChatRoom.get("key").getAsString();
//			        	Log.e("room key", key);
//			        	room.roomID = key.substring(0, key.length() - 7);
//			        	Log.e("room id", room.roomID);
//			        	String value = jsonChatRoom.get("value").getAsString();
////			        	room.addresses = value;
////			        	Log.e("room addresses", room.addresses);
//			        	String[] addr = value.split(",");
//			        	
//			        	for (int j = 0; j < addr.length; j++) {
//			        		User user = GlobalData.getUser(addr[j]);
//			        		room.arrUser.add(user);
//			        		Log.e("user id in room", user.id);
//			        	}
//			        	GlobalData.arrMyRoom.add(room);
//	        		}
//	        		
//	        		MainActivity mainActivity = (MainActivity) parent;
//	        		mainActivity.successMySearchRoomData();
		        }
		    }
		});
	}
}