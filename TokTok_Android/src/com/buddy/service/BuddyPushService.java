package com.buddy.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.buddy.sdk.Buddy;
import com.buddy.sdk.BuddyCallback;
import com.buddy.sdk.BuddyResult;
import com.buddy.sdk.models.User;
import com.chatt.GlobalData;
import com.google.gson.JsonObject;

import android.content.Context;
import android.util.Log;

public class BuddyPushService extends BuddyService {

	public BuddyPushService(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public BuddyPushService(Context context, String _title) {
		super(context, _title);
		// TODO Auto-generated constructor stub
	}
	
	public void sendPush(String type, ArrayList<User> arrToUser, String message) {
		Map<String,Object> parameters = new HashMap<String,Object>();
		
		String strAddresses = "[\"";
		for (int i = 0; i < arrToUser.size(); i++) {
		   
		   User user = arrToUser.get(i);
		   if (i == 0) {
			   strAddresses += user.id;
		   } else {
			   strAddresses += "\",\"" + user.id;
		   }
		}
		strAddresses += "\"]";
		
		Log.e("Addresses", strAddresses);
		
		parameters.put("type", type);
		parameters.put("recipients", strAddresses);
		parameters.put("message", message);
		parameters.put("title", "message");

		double lat = GlobalData.currentLocation.getLatitude();
		double lng = GlobalData.currentLocation.getLongitude();
		
		Buddy.<JsonObject>post("/notifications", parameters, new BuddyCallback<JsonObject>(JsonObject.class) {
		    @Override
		    public void completed(BuddyResult<JsonObject> result) {
		    	
				if (dlgLoading.isShowing())
					dlgLoading.dismiss();
				
		        if (result.getIsSuccess()) {
		            JsonObject obj = result.getResult();
		        }
		    }
		});
	}
}
