package com.buddy.service;

import android.app.ProgressDialog;
import android.content.Context;

public class BuddyService {
	
	public Context parent;
	public ProgressDialog dlgLoading;
	public String title;
	
	public BuddyService(Context context) {
		parent = context;
		
		dlgLoading = new ProgressDialog(parent);
    	dlgLoading.setMessage("\tLoading...");
    	dlgLoading.setCanceledOnTouchOutside(false);
    	dlgLoading.setCancelable(false);
        dlgLoading.show();
	}
	
	public BuddyService(Context context, String _title) {
		parent = context;
		title = _title;
		
		dlgLoading = new ProgressDialog(parent);
    	dlgLoading.setMessage("\t" + _title);
    	dlgLoading.setCanceledOnTouchOutside(false);
    	dlgLoading.setCancelable(false);
        dlgLoading.show();
	}
	
	public BuddyService(Context context, String _title, boolean isShowProgressDialog) {
		parent = context;
		title = _title;
		
		dlgLoading = new ProgressDialog(parent);
    	dlgLoading.setMessage("\t" + _title);
    	dlgLoading.setCanceledOnTouchOutside(false);
    	dlgLoading.setCancelable(false);
    	
    	if (isShowProgressDialog) {
            dlgLoading.show();
    	}
	}
	
	public BuddyService(Context context, boolean isShowProgressDialog) {
		parent = context;
		
		dlgLoading = new ProgressDialog(parent);
    	dlgLoading.setMessage("\tLoading...");
    	dlgLoading.setCanceledOnTouchOutside(false);
    	dlgLoading.setCancelable(false);
    	
    	if (isShowProgressDialog) {
            dlgLoading.show();
    	}
	}
}
