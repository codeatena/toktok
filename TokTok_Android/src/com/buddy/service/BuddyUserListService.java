package com.buddy.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.buddy.sdk.Buddy;
import com.buddy.sdk.BuddyCallback;
import com.buddy.sdk.BuddyResult;
import com.buddy.sdk.models.User;
import com.chatt.GlobalData;
import com.chatt.Login;
import com.chatt.MainActivity;
import com.chatt.UserListActivity;
import com.chatt.model.ChatRoom;
import com.chatt.ui.ChatList;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.han.utility.DialogUtility;
import com.han.utility.TimeUtility;

import android.content.Context;
import android.util.Log;

public class BuddyUserListService extends BuddyService {
	
	ChatRoom curRoom;
			
	public BuddyUserListService(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	public BuddyUserListService(Context context, String _title) {
		super(context, _title);
		// TODO Auto-generated constructor stub
	}
	
	public void createRoom() {
		
		Map<String,Object> parameters = new HashMap<String,Object>();
		
		String roomName = "room" + Long.toString(TimeUtility.getCurrentTimeStamp());
		parameters.put("name", roomName);
		parameters.put("readPermissions", "app");

		Buddy.<JsonObject>post("/users/lists", parameters, new BuddyCallback<JsonObject>(JsonObject.class) {
		    @Override
		    public void completed(BuddyResult<JsonObject> result) {
		    	
				if (dlgLoading.isShowing())
					dlgLoading.dismiss();
				
		        if (result.getIsSuccess()) {
		            JsonObject obj = result.getResult();
		            ChatRoom room = new ChatRoom();

		    		room.roomID = obj.get("id").getAsString();
		    		room.roomName = obj.get("name").getAsString();
		    		
		    		Log.e("created room id", room.roomID);
		    		Log.e("created room name", room.roomName);
		    		
		    		MainActivity mainActivity = (MainActivity) parent;
		    		mainActivity.successCreateChatRoom(room);
		        }
		    }
		});
	}
	
	private void addUserToRoom(ChatRoom chatRoom, User user) {
		Map<String,Object> parameters = new HashMap<String,Object>();
		
//		String roomName = "room" + Long.toString(TimeUtility.getCurrentTimeStamp());
//		parameters.put("name", roomName);

		Buddy.<JsonObject>put("/users/lists/" + chatRoom.roomID + "/items/" + user.id, parameters, new BuddyCallback<JsonObject>(JsonObject.class) {
		    @Override
		    public void completed(BuddyResult<JsonObject> result) {
		    	
				if (dlgLoading.isShowing())
					dlgLoading.dismiss();
				
		        if (result.getIsSuccess()) {
//		            JsonObject obj = result.getResult();
		        }
		    }
		});
	}
	
	public void addUsersToRoom(ChatRoom chatRoom, ArrayList<User> arrUser) {
		
		addUserToRoom(chatRoom, GlobalData.currentUser);
		
		for (User user : arrUser) {
			addUserToRoom(chatRoom, user);
		}
	}
	
	public void searchAllRoom() {
		Map<String,Object> parameters = new HashMap<String,Object>();
		
//		String roomName = "room" + Long.toString(TimeUtility.getCurrentTimeStamp());
//		parameters.put("name", roomName);

		Buddy.<JsonObject>get("/users/lists", parameters, new BuddyCallback<JsonObject>(JsonObject.class) {
		    @Override
		    public void completed(BuddyResult<JsonObject> result) {
		    	
				if (dlgLoading.isShowing())
					dlgLoading.dismiss();
				
		        if (result.getIsSuccess()) {
		        	JsonObject jsonResult = result.getResult();
	        		JsonArray jsonArrRoom = jsonResult.get("pageResults").getAsJsonArray();
	        		
	        		GlobalData.arrAllRoom = new ArrayList<ChatRoom>();
	        		
	        		for (int i = 0; i < jsonArrRoom.size(); i++) {
		        		ChatRoom room = new ChatRoom();
	        			JsonObject jsonRoom = jsonArrRoom.get(i).getAsJsonObject();
	        			room.roomID = jsonRoom.get("id").getAsString();
	        			room.roomName = jsonRoom.get("name").getAsString();
	        			GlobalData.arrAllRoom.add(room);
	        		}
	        		
	        		MainActivity mainActivity = (MainActivity) parent;
	        		mainActivity.successSearchAllRoom();
	        	}
		    }
		});
	}
	
	public void searchUsersInRoom(ChatRoom room) {
		
		curRoom = room;
		
		Map<String,Object> parameters = new HashMap<String,Object>();
		
//		String roomName = "room" + Long.toString(TimeUtility.getCurrentTimeStamp());
//		parameters.put("name", roomName);

		Buddy.<JsonObject>get("/users/lists/" + room.roomID + "/items", parameters, new BuddyCallback<JsonObject>(JsonObject.class) {
//		Buddy.<JsonObject>get("/users/lists/" + "blv.gsfbbkKJrHdr" + "/items", parameters, new BuddyCallback<JsonObject>(JsonObject.class) {

		    @Override
		    public void completed(BuddyResult<JsonObject> result) {
		    	
				if (dlgLoading.isShowing())
					dlgLoading.dismiss();
				
		        if (result.getIsSuccess()) {
		        	JsonObject jsonResult = result.getResult();
	        		JsonArray jsonArrUser = jsonResult.get("pageResults").getAsJsonArray();
	        		
	        		curRoom.arrUser = new ArrayList<User>();
	        		
	        		for (int i = 0; i < jsonArrUser.size(); i++) {
		        		JsonObject jsonUser = jsonArrUser.get(i).getAsJsonObject();
	        			String id = jsonUser.get("id").getAsString();
	        			String type = jsonUser.get("itemType").getAsString();
	        			
	        			if (type.equals("User")) {
	        				User user = GlobalData.getUser(id);
	        				curRoom.arrUser.add(user);
	        			}
	        		}
	        		
	        		MainActivity mainActivity = (MainActivity) parent;
	        		mainActivity.successSearchUsersInRoom();
	        	}
		    }
		});
	}
}