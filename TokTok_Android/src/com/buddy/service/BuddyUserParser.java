package com.buddy.service;

import com.buddy.sdk.models.User;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.han.utility.TimeUtility;

public class BuddyUserParser {
	
	public static User getUserFromJson(JsonObject jsonUser) {
		
		User user = new User();
		
		JsonElement id = jsonUser.get("id");
		JsonElement userName = jsonUser.get("userName");
		JsonElement email = jsonUser.get("email");
		JsonElement firstName = jsonUser.get("firstName");
		JsonElement lastName = jsonUser.get("lastName");
		JsonElement lastLogin = jsonUser.get("lastLogin");
		JsonElement lastModified = jsonUser.get("lastModified");
		JsonElement created = jsonUser.get("created");

		user.id = (id == null) ? null : id.getAsString();
		user.userName = (userName == null) ? null : userName.getAsString();
		user.email = (email == null) ? null : email.getAsString();
		user.firstName = (firstName == null) ? null : firstName.getAsString();
		user.lastName = (lastName == null) ? null : lastName.getAsString();
		user.lastLogin = (lastLogin == null) ? null : TimeUtility.getDateFromJsonString(lastLogin.getAsString());
		user.lastModified = (lastModified == null) ? null : TimeUtility.getDateFromJsonString(lastModified.getAsString());
		user.created = (created == null) ? null : TimeUtility.getDateFromJsonString(created.getAsString());
		
		return user;
	}
}
