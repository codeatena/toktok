package com.buddy.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.buddy.sdk.Buddy;
import com.buddy.sdk.BuddyCallback;
import com.buddy.sdk.BuddyResult;
import com.buddy.sdk.models.User;
import com.chatt.GlobalData;
import com.chatt.Login;
import com.chatt.MainActivity;
import com.chatt.NewChat;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
//import com.google.gson.JsonObject;
import com.han.utility.DialogUtility;
import com.han.utility.TimeUtility;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BuddyUserService extends BuddyService {

	public BuddyUserService(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	public BuddyUserService(Context context, String _title) {
		super(context, _title);
	}
	
	public void createUser(String email, String password) {
		String userName = "UserID" + Long.toString(TimeUtility.getCurrentTimeStamp());
		Buddy.createUser(userName, password, null, null, email, null, null, null, new BuddyCallback<User>(User.class) {
			@Override
			public void completed(BuddyResult<User> result) {
				//  complete register process
				if (dlgLoading.isShowing())
					dlgLoading.dismiss();
				
				if (result.getIsSuccess()) {
					//  success register via buddy
					DialogUtility.showToast(parent, "You created account on Buddy");
				} else {
					//  fail register via buddy
					DialogUtility.showToast(parent, result.getError());
				}
			}
		});
	}
	
	public void searchUserByEmail(String email) {
		
		if (email.equals("") || email == null) {
			DialogUtility.showToast(parent, "Please input email");
			return;
		}
		
		Map<String,Object> parameters = new HashMap<String,Object>();
		parameters.put("email", email);

		Buddy.<JsonObject>get("/users", parameters, new BuddyCallback<JsonObject>(JsonObject.class) {
		    @Override
		    public void completed(BuddyResult<JsonObject> result) {
				if (dlgLoading.isShowing())
					dlgLoading.dismiss();
				
		        if (result.getIsSuccess()) {
		        	try {
		        		JsonObject jsonResult = result.getResult();
		        		JsonArray jsonArrUser = jsonResult.get("pageResults").getAsJsonArray();
		        		
		        		JsonObject jsonUser = null;
		        		if (jsonArrUser.size() > 0) {
		        			jsonUser = jsonArrUser.get(0).getAsJsonObject();
		        		}
		        		completeSearchByEmail(jsonUser);
		        	} catch (Exception e) {
		        		
		        	}
		        }
		    }
		});
	}
	
	public void generalLogin(String userName, String password) {
		
		Buddy.loginUser(userName, password, new BuddyCallback<User>(User.class) {
			@Override
			public void completed(BuddyResult<User> result) {
				//  complete login process
				if (dlgLoading.isShowing())
					dlgLoading.dismiss();
				
				if (result.getIsSuccess()) {
					//  success login via buddy
					DialogUtility.showToast(parent, "Login success on Buddy");
					GlobalData.currentUser = result.getResult();
					
		            Login loginActivity = (Login) parent;
					loginActivity.successLogin();
				} else {
					//  fail login via buddy
					DialogUtility.showToast(parent, result.getError());
				}
			}
		});
	}
	
	public void getUserInf(String id) {
		Buddy.<JsonObject>get("/users/" + id, null, new BuddyCallback<JsonObject>(JsonObject.class) {
		    @Override
		    public void completed(BuddyResult<JsonObject> result) {
				if (dlgLoading.isShowing())
					dlgLoading.dismiss();
		        if (result.getIsSuccess()) {
		        	try {
		        		JsonObject obj = result.getResult();
		        		complteGetUserInf(obj);
		        	} catch (Exception e) {
		        		
		        	}
		        }
		    }
		});
	}
	
	public void facebookLogin(String fbID, String fbAccessToken) {
		
		Map<String,Object> parameters = new HashMap<String,Object>();
		
		parameters.put("identityProviderName", "Facebook");
		parameters.put("identityID", fbID);
		parameters.put("identityAccessToken", fbAccessToken);

		Buddy.<JsonObject>post("/users/login/social", parameters, new BuddyCallback<JsonObject>(JsonObject.class) {
		    @Override
		    public void completed(BuddyResult<JsonObject> result) {
		    	
				if (dlgLoading.isShowing())
					dlgLoading.dismiss();
				
		        if (result.getIsSuccess()) {
		            JsonObject obj = result.getResult();
		            // get the ID of the created user.
		            User user = BuddyUserParser.getUserFromJson(obj);
		            GlobalData.currentUser = user;
					DialogUtility.showToast(parent, "Success login via Facebook");

		            Login loginActivity = (Login) parent;
					loginActivity.successLogin();
		        }
		    }
		});
	}
	
	public void searchAllUser() {
		Map<String,Object> parameters = new HashMap<String,Object>();

		Buddy.<JsonObject>get("/users", parameters, new BuddyCallback<JsonObject>(JsonObject.class) {
		    @Override
		    public void completed(BuddyResult<JsonObject> result) {
				if (dlgLoading.isShowing())
					dlgLoading.dismiss();
				
		        if (result.getIsSuccess()) {
		        	try {
		        		JsonObject jsonResult = result.getResult();
		        		JsonArray jsonArrUser = jsonResult.get("pageResults").getAsJsonArray();
		        		
		        		GlobalData.arrAllUser = new ArrayList<User>();
	        			Log.e("current user id", GlobalData.currentUser.id);

		        		for (int i = 0; i < jsonArrUser.size(); i++) {
		        			JsonObject jsonUser = jsonArrUser.get(i).getAsJsonObject();
		        			User user = BuddyUserParser.getUserFromJson(jsonUser);
	        				GlobalData.arrAllUser.add(user);
		        		}
		        		
		        		MainActivity mainActivity = (MainActivity) parent;
		        		mainActivity.successSearchAllUser();
		        		
		        	} catch (Exception e) {
		        		
		        	}
		        }
		    }
		});
	}
	
	private void completeSearchByEmail(JsonObject jsonUser) {
		if (jsonUser != null) {
			if (parent instanceof Login) {
				Login loginActivity = (Login) parent;
				User user = BuddyUserParser.getUserFromJson(jsonUser);
				loginActivity.successSearchUserByEmail(user);
			}
			if (parent instanceof NewChat) {
				NewChat newChatActivity = (NewChat) parent;
				User user = BuddyUserParser.getUserFromJson(jsonUser);
				newChatActivity.successSearchUserByEmail(user);
			}
		} else {
			DialogUtility.showToast(parent, "This user doesnt exist");
			if (parent instanceof NewChat) {
				NewChat newChatActivity = (NewChat) parent;
				newChatActivity.failSearchUserByEmail();
			}
		}
	}
	
	private void complteGetUserInf(JsonObject jsonUser) {
		if (jsonUser != null) {
			if (parent instanceof NewChat) {
				User user = BuddyUserParser.getUserFromJson(jsonUser);
				
				NewChat newChatActivity = (NewChat) parent;
				newChatActivity.successGetUserInf(user);
			}
		} else {
			
		}
	}
	
	public static void logoutUser() {
		Buddy.logoutUser(new BuddyCallback<Boolean>(Boolean.class) {
			@Override
			public void completed(BuddyResult<Boolean> result) {
				
			}
		});
	}
}
