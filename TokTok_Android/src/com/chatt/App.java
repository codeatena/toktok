package com.chatt;

import com.buddy.sdk.Buddy;
import com.buddy.service.BuddySetting;
import com.han.utility.PreferenceUtility;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

public class App extends Application {

    private static App instance;
    
    public static App getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initApplication();
    }

    private void initApplication() {
        instance = this;

        PreferenceUtility.sharedPreferences = getSharedPreferences(PreferenceUtility.PREF_NAME, MODE_PRIVATE);
		Buddy.init(this, BuddySetting.BUDDY_APP_ID, BuddySetting.BUDDY_APP_KEY);
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    public int getAppVersion() {
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
}