package com.chatt;

import java.util.ArrayList;

import android.location.Location;

import com.buddy.sdk.models.User;
import com.chatt.model.ChatRoom;

public class GlobalData {

	public static User currentUser = new User();
	public static Location currentLocation;
	
	public static ArrayList<User> arrAllUser = new ArrayList<User>();
	public static ArrayList<ChatRoom> arrMyRoom = new ArrayList<ChatRoom>();
	public static ArrayList<ChatRoom> arrAllRoom = new ArrayList<ChatRoom>();

	public static String getCurrentUserFullName() {
		
		String fullName = "";
		fullName = currentUser.userName;
		
		if (currentUser.firstName != null) {
			if (currentUser.lastName != null) {
				fullName = currentUser.firstName + " " + currentUser.lastName;
			}
		}
		
		return fullName;
	}
	
	public static String getCurrentLocationString() {
		double lat = GlobalData.currentLocation.getLatitude();
		double lng = GlobalData.currentLocation.getLongitude();
		
		return Double.toString(lat) + "," + Double.toString(lng);
	}
	
	public static User getUser(String userID) {
		
		for (User user : arrAllUser) {
			if (userID.equals(user.id)) {
				return user;
			}
		}
		
		return null;
	}
}
