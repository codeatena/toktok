package com.chatt;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.buddy.sdk.Buddy;
import com.buddy.sdk.BuddyCallback;
import com.buddy.sdk.BuddyResult;
import com.buddy.sdk.models.User;
import com.buddy.service.BuddyDeviceService;
import com.buddy.service.BuddyUserListService;
import com.buddy.service.BuddyUserService;
import com.chatt.custom.CustomActivity;
import com.chatt.push.PlayServicesHelper;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;
import com.han.utility.DialogUtility;
import com.han.utility.LocationUtility;
import com.han.utility.PreferenceUtility;

/**
 * The Class Login is an Activity class that shows the login screen to users.
 * The current implementation simply start the MainActivity. You can write your
 * own logic for actual login and for login using Facebook and Twitter.
 */
public class Login extends CustomActivity implements LocationListener,
		GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener {

	/* (non-Javadoc)
	 * @see com.chatt.custom.CustomActivity#onCreate(android.os.Bundle)
	 */
	private LocationRequest mLocationRequest;

    // Stores the current instantiation of the location client in this object
    private LocationClient mLocationClient;
    
	private LocationManager locationManager;
	
	EditText edtEmail;  // EditText for inputing username
	EditText edtPassword;  // EditText for inputing password
	String fbAccessToken;
	
    private PlayServicesHelper playServicesHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		
		try {
			PackageInfo info = getPackageManager().getPackageInfo(
					"com.chatt", PackageManager.GET_SIGNATURES);
			
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.e("KeyHash:",
						Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {

		} catch (NoSuchAlgorithmException e) {

		}
		
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
		edtEmail = (EditText) findViewById(R.id.user_name_editText);
		edtPassword = (EditText) findViewById(R.id.password_editText);
	}
	
	private void initValue() {
		BuddyUserService.logoutUser();

		mLocationRequest = LocationRequest.create();

        /*
         * Set the update interval
         */
        mLocationRequest.setInterval(LocationUtility.UPDATE_INTERVAL_IN_MILLISECONDS);

        // Use high accuracy
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Set the interval ceiling to one minute
        mLocationRequest.setFastestInterval(LocationUtility.FAST_INTERVAL_CEILING_IN_MILLISECONDS);

        mLocationClient = new LocationClient(this, this, this);
        
        playServicesHelper = new PlayServicesHelper(this);
        if (!playServicesHelper.checkPlayServices()) {
        	finish();
        }
        
        String email = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_USER_EMAIL, "");
        String password = PreferenceUtility.sharedPreferences.getString(PreferenceUtility.PREFERENCE_KEY_USER_PASSWORD, "");
        
        edtEmail.setText(email);
        edtPassword.setText(password);
	}
	
	private void initEvent() {
		setTouchNClick(R.id.btnLogin);
		setTouchNClick(R.id.btnFb);
		setTouchNClick(R.id.btnTw);
		setTouchNClick(R.id.btnReg);
		
		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
	   	 
        // Creating a criteria object to retrieve provider
        Criteria criteria = new Criteria();

        // Getting the name of the best provider
        String provider = locationManager.getBestProvider(criteria, true);

        // Getting Current Location
        Location location = locationManager.getLastKnownLocation(provider);
//        searchAction(location);
        
        if(location != null) {
            onLocationChanged(location);
        }
        
        locationManager.requestLocationUpdates(provider, 200, 1, this);
	}

	/* (non-Javadoc)
	 * @see com.chatt.custom.CustomActivity#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v) {
		String email 	= edtEmail.getText().toString();
		String password 	= edtPassword.getText().toString();
		
		super.onClick(v);
		if (v.getId() == R.id.btnLogin) {
			//  search user with this email 
			new BuddyUserService(this).searchUserByEmail(email);
		}
		if (v.getId() == R.id.btnReg) {
			//  click register button
//			new BuddyUserService(this).createUser(email, password);			
		}
		if (v.getId() == R.id.btnFb) {
			List<String> permissions = new ArrayList<String>();
			permissions.add("email");
			Session.openActiveSession(this, true, permissions, new Session.StatusCallback() {

			  // callback when session changes state
				@Override
				public void call(Session session, SessionState state, Exception exception) {
					if (session.isOpened()) {
						fbAccessToken = session.getAccessToken();
						// make request to the /me API
						Request.newMeRequest(session, new Request.GraphUserCallback() {
			
							// callback after Graph API response with user object
							@Override
							public void onCompleted(GraphUser user, Response response) {
								if (user != null) {				  
									try {
										Log.e("facebook", "username: " + user.getName());
										Log.e("facebook", "email: " + user.getProperty("email").toString());
										Log.e("facebook", "ID: " + user.getId());
										Log.e("facebook", "accessToken: " + fbAccessToken);
										
										new BuddyUserService(Login.this).facebookLogin(user.getId(), fbAccessToken);
									} catch (Exception e) {
										
									}
								}
							}
						}).executeAsync();
					}
				}
			});
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	    Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
	}
	
	public void successSearchUserByEmail(User user) {
		
		//  If user exist with email, process general login
		String password 	= edtPassword.getText().toString();
		new BuddyUserService(this).generalLogin(user.userName, password);
	}
	
	public void successLogin() {
		Buddy.setLastLocation(GlobalData.currentLocation);
		
		Editor editor = PreferenceUtility.sharedPreferences.edit();
		
		String email = GlobalData.currentUser.email;
		String password = edtPassword.getText().toString();
		
		editor.putString(PreferenceUtility.PREFERENCE_KEY_USER_EMAIL, email);
		editor.putString(PreferenceUtility.PREFERENCE_KEY_USER_PASSWORD, password);
		editor.commit();
				
		startActivity(new Intent(this, MainActivity.class));
		finish();
	}
	
	public void searchAction(Location location) {
		
		if (location == null) return;
		
		String strLat = Double.toString(location.getLatitude());
		String strLng = Double.toString(location.getLongitude());
		
	    GlobalData.currentLocation = location;
		Log.e("Latitude, Longitude = ", strLat + " " + strLng);
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
        Toast.makeText(this, "GPS Connected", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
        Toast.makeText(this, "GPS Disconnected.", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		if (location == null) return;
		
	    GlobalData.currentLocation = location;
	    searchAction(location);
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}
	
    @Override
    protected void onResume() {
        super.onResume();
        playServicesHelper.checkPlayServices();
    }
	
	public void onStart() {
        super.onStart();
        mLocationClient.connect();
	}
	
	public void onStop() {
        mLocationClient.disconnect();
	    locationManager.removeUpdates(this);
	    super.onStop();
	}
}