package com.chatt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.buddy.sdk.Buddy;
import com.buddy.sdk.BuddyCallback;
import com.buddy.sdk.BuddyResult;
import com.buddy.sdk.models.User;
import com.buddy.service.BuddyMessageService;
import com.buddy.service.BuddyPushService;
import com.buddy.service.BuddyUserService;
import com.chatt.custom.CustomActivity;
import com.google.gson.JsonObject;
import com.han.utility.DialogUtility;

/**
 * The Class NewChat is an Activity class that shows the screen for creating a
 * new Chat. The current implementation simply shows the layout and you can
 * apply your logic for each button click and for other view components.
 */
public class NewChat extends CustomActivity
{

	/* (non-Javadoc)
	 * @see com.chatt.custom.CustomActivity#onCreate(android.os.Bundle)
	 */
	EditText edtSendTo; 
	EditText edtSendMessage;
	TextView txtAddresses;
	
	ArrayList<User> arrGroupUser;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.new_chat);

		initWidget();
		initValue();
		initEvent();
		
		getActionBar().setLogo(R.drawable.icon_trans);
	}

	/* (non-Javadoc)
	 * @see com.chatt.custom.CustomActivity#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v)
	{
		super.onClick(v);
		if (v.getId() == R.id.btnAdd) {
			String strSendTo = edtSendTo.getText().toString();
//			String strMessage = edtSendMessage.getText().toString();
			if (strSendTo.equals("") || strSendTo == null) {
				DialogUtility.showToast(this, "Please input address.");
				return;
			}
			for (User tmpUser : arrGroupUser) {
				if (strSendTo.equals(tmpUser.email)) {
					DialogUtility.showToast(this, "You added this user aleady.");
					return;
				}
			}
			new BuddyUserService(this).searchUserByEmail(strSendTo);
		}
		if (v.getId() == R.id.btnSend) {
			String strMessage = edtSendMessage.getText().toString();
			if (arrGroupUser.size() == 0) {
				DialogUtility.showToast(this, "Please input email address you want to send message.");
				return;
			}
			if (strMessage.equals("") || strMessage == null) {
				DialogUtility.showToast(this, "Please input message.");
				return;
			}
			new BuddyMessageService(this).sendMessage(arrGroupUser, "Invite", strMessage, null, null);
			new BuddyPushService(this).sendPush("Alert", arrGroupUser, strMessage);
		}
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.newchat, menu);
		return super.onCreateOptionsMenu(menu);
	}

	/* (non-Javadoc)
	 * @see com.newsfeeder.custom.CustomActivity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if (item.getItemId() == android.R.id.home)
			finish();
		return super.onOptionsItemSelected(item);
	}
	
	private void initWidget() {
		edtSendTo 	= (EditText) findViewById(R.id.send_to_textView);
		edtSendMessage 	= (EditText) findViewById(R.id.send_message_editText);
		txtAddresses = (TextView) findViewById(R.id.address_textView);
		
		Location location = Buddy.getLastLocation();
		
		Log.e("last location", Double.toString(location.getLatitude()) + ", "
				+ Double.toString(location.getLongitude()));
	}
	
	private void initValue() {
		arrGroupUser = new ArrayList<User>();
	}
	
	private void initEvent() {
		setTouchNClick(R.id.btnAdd);
		setTouchNClick(R.id.btnProject);
		setTouchNClick(R.id.btnCamera);
		setTouchNClick(R.id.btnSend);
	}
	
	private void addUserInGroup(User user) {
//		user.email = edtSendTo.getText().toString();
		
		arrGroupUser.add(user);
		String str = "Addresses: ";
		
		for (int i = 0; i < arrGroupUser.size(); i++) {
			User tmpUser = arrGroupUser.get(i);
			if (i == 0) {
				str += tmpUser.email;
			} else {
				str += ", " + tmpUser.email;
			}
		}
		txtAddresses.setText(str);
	}
	
	private void getUserInf(User user) {
		new BuddyUserService(this).getUserInf(user.id);
	}
	
	public void successSearchUserByEmail(User user) {
		getUserInf(user);
	}
	
	public void failSearchUserByEmail() {
		DialogUtility.showGeneralAlert(this, "Notification!", edtSendTo.getText().toString() + 
				" is not a member of TokTok - we�ll send them an invite.");
	}
	
	public void successGetUserInf(User user) {
		addUserInGroup(user);
	}
}
