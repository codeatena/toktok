package com.chatt;

import java.util.ArrayList;

import com.buddy.sdk.models.User;
import com.chatt.custom.CustomActivity;
import com.chatt.model.UserListItem;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

public class UserListActivity extends CustomActivity {

	ListView lstUser;
	ArrayList<UserListItem> arrUserListItem;
	UserAdapter userAdapter;
	
	public static ArrayList<User> arrUserResult = new ArrayList<User>();
	
	public static final int 	SELECT_CHECKBOX_ID = 100;
	
	public static final int 	REQUEST_CODE_CREATE_USER_LIST = 1001;
	public static final int 	REQUEST_CODE_ADD_USER_LIST = 1002;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_list);

		initWidget();
		initValue();
		initEvent();
		
		getActionBar().setTitle("UserList");
		getActionBar().setLogo(R.drawable.icon_trans);
	}
	
	private void initWidget() {
		lstUser = (ListView) findViewById(R.id.user_listView);
	}
	
	private void initValue() {
		
		arrUserListItem = new ArrayList<UserListItem>();
		for (User user : GlobalData.arrAllUser) {
			
			if (user.id.equals(GlobalData.currentUser.id)) 
				continue;
			
			UserListItem userListItem = new UserListItem();
			userListItem.isSelect = false;
			userListItem.user = user;
			
			arrUserListItem.add(userListItem);
		}
		
		userAdapter = new UserAdapter(this, R.layout.row_user);
		lstUser.setAdapter(userAdapter);
	}
	
	private void initEvent() {

	}
	
	public void onClick(View v)	{
		super.onClick(v);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.menu_user_list, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if (item.getItemId() == android.R.id.home)
			finish();
		
		if (item.getItemId() == R.id.menu_done) {
			arrUserResult = new ArrayList<User>();
			for (UserListItem userListItem : arrUserListItem) {
				if (userListItem.isSelect) {
					arrUserResult.add(userListItem.user);
					Log.e(userListItem.user.userName, "True");
				}
			}
			if (arrUserResult.size() > 0) {
				setResult(RESULT_OK);
			}
			finish();
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	public class UserAdapter extends ArrayAdapter<UserListItem> {
    	
//    	ArrayList<UserListItem> arrData;
    	int row_id;

		public UserAdapter(Context context, int _row_id) {
			super(context, _row_id, arrUserListItem);
			// TODO Auto-generated constructor stub
			row_id = _row_id;
//			arrData = _arrData;
		}
		
		public View getView(int position, View convertView, ViewGroup parent) {
			
	        View row = convertView;
	        LayoutInflater inflater = UserListActivity.this.getLayoutInflater();
	        row = inflater.inflate(row_id, parent, false);
	        
	        TextView txtUserName = (TextView) row.findViewById(R.id.user_name_textView);
	        TextView txtEmail = (TextView) row.findViewById(R.id.user_email_textView);
	        TextView txtUserID = (TextView) row.findViewById(R.id.user_id_textView);
	        CheckBox chkSelect = (CheckBox) row.findViewById(R.id.user_select_checkBox);
	        
	        UserListItem userListItem = arrUserListItem.get(position);
	        
	        if (userListItem.user.userName != null)
	        	txtUserName.setText(userListItem.user.userName);
	        else
	        	txtUserName.setText("Username is not Specified yet");
	        
	        if (userListItem.user.email != null)
	        	txtEmail.setText(userListItem.user.email);
	        else
	        	txtEmail.setText("Email is not Specified yet");
	        
	        if (userListItem.user.id != null)
	        	txtUserID.setText(userListItem.user.id);
	        else
	        	txtUserID.setText("User id is not Specified yet");
	        
	        chkSelect.setChecked(userListItem.isSelect);
	        chkSelect.setId(SELECT_CHECKBOX_ID + position);
	        
	        chkSelect.setOnClickListener( new View.OnClickListener() {  
	            public void onClick(View v) {  
	            	CheckBox cb = (CheckBox) v;
	            	int pos = cb.getId() - SELECT_CHECKBOX_ID;
	            	UserListItem userListItem = arrUserListItem.get(pos);
	            	userListItem.isSelect = cb.isChecked();
	            }
	        });
	        
	        return row;
		}
    }
}
