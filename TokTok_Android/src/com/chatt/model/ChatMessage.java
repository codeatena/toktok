package com.chatt.model;

import java.util.Date;

import com.buddy.sdk.models.User;

public class ChatMessage {
	
	public String id;
	public String body;
	public String subject;
	public User sender;
	public Date dtCreated;
	public Date dtSent;
	public Date dtLastModified;
	public boolean isNew;
}