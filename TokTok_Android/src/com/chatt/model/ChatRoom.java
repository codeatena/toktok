package com.chatt.model;

import java.util.ArrayList;

import com.buddy.sdk.models.User;
import com.chatt.GlobalData;

public class ChatRoom {

	public ArrayList<User> arrUser = new ArrayList<User>();
	public String roomID;
	public String roomName;
//	public String addresses;
	
	public String getAddresses() {
		String addresses = "";
		for (int i = 0; i < arrUser.size(); i++) {
			User user = arrUser.get(i);
			String addr = "";
			
			addr = user.id;
			if (user.userName != null) addr = user.userName;
			if (user.email != null) addr = user.email;
			
			if (i == 0) {
				addresses += addr;
			} else {
				addresses += "," + addr;
			}
		}
		return addresses;
	}
	
	public String getAddressesExceptMe() {
		String addresses = "";
		for (int i = 0; i < arrUser.size(); i++) {
			User user = arrUser.get(i);
			String addr = "";
			
			addr = user.id;
			if (addr.equals(GlobalData.currentUser.id)) continue;
			if (user.userName != null) addr = user.userName;
			if (user.email != null) addr = user.email;
			
			addresses += "," + addr;
		}
		
		addresses = addresses.substring(1);
		return addresses;
	}

	public ArrayList<User> getUsersExceptMe() {
		
		ArrayList<User> arrGroupUser = new ArrayList<User>();
		for (User user : arrUser) {
			if (!user.id.equals(GlobalData.currentUser.id)) {
				arrGroupUser.add(user);
			}
		}
		
		return arrGroupUser;
	}
 }
