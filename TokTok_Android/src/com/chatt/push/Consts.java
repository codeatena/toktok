package com.chatt.push;

public class Consts {

    // In GCM, the Sender ID is a project ID that you acquire from the API console
    public static final String PROJECT_NUMBER = "741933782735";
    
    public static final String EXTRA_MESSAGE = "message";

    public static final String GCM_NOTIFICATION = "TokTok";
    public static final String GCM_DELETED_MESSAGE = "Deleted messages on server: ";
    public static final String GCM_INTENT_SERVICE = "GcmIntentService";
    public static final String GCM_SEND_ERROR = "Send error: ";
    public static final String GCM_RECEIVED = "Received: ";
}