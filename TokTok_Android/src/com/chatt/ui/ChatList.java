package com.chatt.ui;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.buddy.service.BuddyMetaDataService;
import com.buddy.service.BuddyUserListService;
import com.chatt.GlobalData;
import com.chatt.MainActivity;
import com.chatt.NewChat;
import com.chatt.R;
import com.chatt.UserListActivity;
import com.chatt.custom.CustomFragment;
import com.chatt.model.ChatItem;
import com.chatt.model.ChatRoom;

/**
 * The Class ChatList is the Fragment class that is launched when the user
 * clicks on Chats option in Left navigation drawer. It shows a dummy list of
 * user's chats. You need to write your own code to load and display actual
 * chat.
 */
public class ChatList extends CustomFragment
{

	/** The Chat list. */
	private ArrayList<ChatItem> chatList;
	ListView list;
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)	{
		View v = inflater.inflate(R.layout.chat_list, null);

		list = (ListView) v.findViewById(R.id.list);
		loadChatList(GlobalData.arrMyRoom);
		list.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3)	{
				
				ChatItem chatItem = chatList.get(pos);
				
				MainActivity mainActivity = (MainActivity) getActivity();
				mainActivity.showGroupChat(chatItem.room);
			}
		});

		setTouchNClick(v.findViewById(R.id.tab1));
		setTouchNClick(v.findViewById(R.id.tab2));
		setTouchNClick(v.findViewById(R.id.btnNewChat));
		
		return v;
	}

	/* (non-Javadoc)
	 * @see com.socialshare.custom.CustomFragment#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v)	{
		super.onClick(v);
		
		if (v.getId() == R.id.tab1)	{
			loadChatList(GlobalData.arrMyRoom);
			getView().findViewById(R.id.tab2).setEnabled(true);
			v.setEnabled(false);
		} else if (v.getId() == R.id.tab2) {
			loadChatList(GlobalData.arrAllRoom);
			getView().findViewById(R.id.tab1).setEnabled(true);
			v.setEnabled(false);
		} else if (v.getId() == R.id.btnNewChat)
			startActivityForResult(new Intent(getActivity(), UserListActivity.class), UserListActivity.REQUEST_CODE_CREATE_USER_LIST);
	}

	/**
	 * This method currently loads a dummy list of chats. You can write the
	 * actual implementation of loading chats.
	 */
	private void loadChatList(ArrayList<ChatRoom> arrRoom) {
		ArrayList<ChatItem> chatList = new ArrayList<ChatItem>();
		
		for (ChatRoom room : arrRoom) {
			chatList.add(new ChatItem(room, room.getAddressesExceptMe(), "My special project",
				"", "", R.drawable.user1, true,
				true));
		}

		this.chatList = new ArrayList<ChatItem>(chatList);
		list.setAdapter(new ChatAdapter());
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == UserListActivity.REQUEST_CODE_CREATE_USER_LIST) {
			if (resultCode == Activity.RESULT_OK) {
				new BuddyUserListService(getActivity()).createRoom();
//				startActivity(new Intent(getActivity(), NewChat.class));
			}
		}
	}

	private class ChatAdapter extends BaseAdapter {
		@Override
		public int getCount() {
			return chatList.size();
		}

		@Override
		public ChatItem getItem(int arg0) {
			return chatList.get(arg0);
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getItemId(int)
		 */
		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
		 */
		@Override
		public View getView(int pos, View v, ViewGroup arg2) {
			if (v == null)
				v = LayoutInflater.from(getActivity()).inflate(
						R.layout.chat_item, null);

			ChatItem c = getItem(pos);
			TextView lbl = (TextView) v.findViewById(R.id.user_name_textView);
			lbl.setText(c.getName());

			lbl = (TextView) v.findViewById(R.id.lbl2);
			lbl.setText(c.getDate());

			lbl = (TextView) v.findViewById(R.id.user_email_textView);
			lbl.setText(c.getTitle());

			lbl = (TextView) v.findViewById(R.id.user_id_textView);
			lbl.setText(c.getMsg());

			ImageView img = (ImageView) v.findViewById(R.id.img1);
			img.setImageResource(c.getIcon());

			img = (ImageView) v.findViewById(R.id.img2);
			img.setImageResource(c.isGroup() ? R.drawable.ic_group : R.drawable.ic_lock);

			img = (ImageView) v.findViewById(R.id.online);
			img.setVisibility(c.isOnline() ? View.VISIBLE : View.INVISIBLE);
			
			return v;
		}
	}
}