package com.chatt.ui;

import java.util.ArrayList;

import android.content.Context;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.buddy.service.BuddyMessageService;
import com.buddy.service.BuddyPushService;
import com.chatt.GlobalData;
import com.chatt.R;
import com.chatt.custom.CustomFragment;
import com.chatt.model.ChatMessage;
import com.chatt.model.ChatRoom;
import com.chatt.model.Conversation;
import com.han.utility.DialogUtility;
import com.han.utility.TimeUtility;

/**
 * The Class GroupChat is the Fragment class that is launched when the user
 * clicks on a Chat item in ChatList fragment. The current implementation simply
 * shows dummy conversations and when you send a chat message it will show a
 * dummy auto reply message. You can write your own code for actual Chat.
 */
public class GroupChat extends CustomFragment
{

	/** The Conversation list. */
	private ArrayList<Conversation> convList;

	/** The chat adapter. */
	private ChatAdapter adp;

	/** The Editext to compose the message. */
	private EditText txt;
	
	TextView txtSendTo;
	
	public ChatRoom chatRoom; 

	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.group_chat, null);

		loadConversationList();
		ListView list = (ListView) v.findViewById(R.id.list);
		adp = new ChatAdapter();
		list.setAdapter(adp);
		list.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
		list.setStackFromBottom(true);

		txt = (EditText) v.findViewById(R.id.send_message_editText);
		txt.setInputType(InputType.TYPE_CLASS_TEXT
				| InputType.TYPE_TEXT_FLAG_MULTI_LINE);
		
		txtSendTo = (TextView) v.findViewById(R.id.send_to_textView);
		txtSendTo.setText(chatRoom.getAddressesExceptMe());
		initValue();
		
		setTouchNClick(v.findViewById(R.id.btnAdd));
		setTouchNClick(v.findViewById(R.id.btnEdit));
		setTouchNClick(v.findViewById(R.id.btnCamera));
		setTouchNClick(v.findViewById(R.id.btnSend));
		
		return v;
	}

	/* (non-Javadoc)
	 * @see com.socialshare.custom.CustomFragment#onClick(android.view.View)
	 */
	
	public void initValue() {
		new BuddyMessageService(getActivity(), false).searchMessageInRoom(chatRoom);
	}
	
	@Override
	public void onClick(View v) {
		super.onClick(v);
		if (v.getId() == R.id.btnSend)
		{
			sendMessage();
		}

	}

	/**
	 * Call this method to Send message to opponent. The current implementation
	 * simply add an auto reply message with each sent message.
	 * You need to write actual logic for sending and receiving the messages.
	 */
	private void sendMessage() {
		if (txt.length() == 0)
			return;

		InputMethodManager imm = (InputMethodManager) getActivity()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(txt.getWindowToken(), 0);
		
		String strMessage = txt.getText().toString();

		if (strMessage.equals("") || strMessage == null) {
			DialogUtility.showToast(getActivity(), "Please input message.");
			return;
		}
		
		new BuddyMessageService(getActivity()).sendMessage(chatRoom.getUsersExceptMe(), "message", strMessage, null, chatRoom.roomID);
		new BuddyPushService(getActivity()).sendPush("Alert", chatRoom.getUsersExceptMe(), strMessage);
		
		convList.add(new Conversation(strMessage, TimeUtility.getCurrentTimeAsFormat("yyyy-MM-dd HH:mm"), true, true));
//		initValue();

		adp.notifyDataSetChanged();
		txt.setText(null);
	}

	/**
	 * This method currently loads a dummy list of conversations. You can write the
	 * actual implementation of loading conversations.
	 */
	private void loadConversationList()	{
		convList = new ArrayList<Conversation>();
	}

	/**
	 * The Class CutsomAdapter is the adapter class for Chat ListView. The
	 * currently implementation of this adapter simply display static dummy
	 * contents. You need to write the code for displaying actual contents.
	 */
	private class ChatAdapter extends BaseAdapter {

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getCount()
		 */
		@Override
		public int getCount()
		{
			return convList.size();
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getItem(int)
		 */
		@Override
		public Conversation getItem(int arg0)
		{
			return convList.get(arg0);
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getItemId(int)
		 */
		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		/* (non-Javadoc)
		 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
		 */
		@Override
		public View getView(int pos, View v, ViewGroup arg2) {
			Conversation c = getItem(pos);
			if (c.isSent())
				v = LayoutInflater.from(getActivity()).inflate(
						R.layout.chat_item_sent, null);
			else
				v = LayoutInflater.from(getActivity()).inflate(
						R.layout.chat_item_rcv, null);

			TextView lbl = (TextView) v.findViewById(R.id.user_name_textView);
			lbl.setText(c.getDate() + "\n" + c.getMsg());

//			lbl = (TextView) v.findViewById(R.id.lbl2);
//			lbl.setText(c.getMsg());

//			lbl = (TextView) v.findViewById(R.id.user_email_textView);
//			
//			if (c.isSuccess())
//				lbl.setText("Delivered");
//			else
//				lbl.setText("");

			return v;
		}
	}
	
	public void redrawMessage(ArrayList<ChatMessage> arrChatMessage) {
		convList.clear();
		for (ChatMessage chatMessage : arrChatMessage) {
			
			String strDate = TimeUtility.getStringFromDate(chatMessage.dtLastModified, "yyyy-MM-dd HH:mm");
			
			if (chatMessage.sender.id.equals(GlobalData.currentUser.id)) {
				convList.add(new Conversation(chatMessage.body, strDate, true, true));
			} else {
				convList.add(new Conversation(chatMessage.body, strDate, false, true));
			}
		}
		adp.notifyDataSetChanged();
	}
}
