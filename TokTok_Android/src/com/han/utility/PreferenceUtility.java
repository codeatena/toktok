package com.han.utility;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class PreferenceUtility {
	
	public static final String PREF_NAME = "toktok_pref";
	
    public static final String PREFERENCE_KEY_APP_VERSION = "appVersion";
    public static final String PREFERENCE_KEY_REG_ID = "registration_id";
    
    public static final String PREFERENCE_KEY_USER_EMAIL = "user_email";
    public static final String PREFERENCE_KEY_USER_PASSWORD = "user_password";
    
	public static SharedPreferences sharedPreferences;
	public static Editor prefEditor;
}